<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_produk extends CI_Model
{
    public function all()
    {
        $query = "SELECT `pelelangan`.*,`cabang`.`cabang`
                FROM `pelelangan` JOIN `cabang`
                ON `pelelangan`.`id_cabang` = `cabang`.`id`
        ";
        return $this->db->query($query)->result_array();
    }

    // public function barang()
    // {
    //     $product_id = $this->db->get('id_barang');
    //     // $product_id = $this->get('product_id');
    //     if ($product_id == '') {
    //         $this->db->join('cabang c', 'p.id_barang = c.id');
    //         $this->db->order_by("id_barang", "desc");
    //         $product = $this->db->get('pelelangan p')->result();
    //     } else {
    //         $this->db->join('cabang c', 'p.id_barang = c.id_barang');
    //         $this->db->where('id_barang', $product_id);
    //         $product = $this->db->get('pelelangan p')->result();
    //     }
    // }

    public function allcabang($id)
    {
        // $id = array(
        //     'uri' => $this->db->get('uri')
        // );
        // print_r($uri); die;

        // $ida = $id['uri'];
        $this->db->where('p.id_cabang', $id);
        $this->db->join('cabang c', 'p.id_cabang = c.id');
        // $this->db->like('category',$key);
        $product = $this->db->get('pelelangan p')->result_array();
        return $product;
    }


    public function allspec($key)
    {
        // $search = array(
        //     'search' => $this->db->get('search')
        // );
        // $key = $search['search'];
        $this->db->like('nama_barang', $key);
        $this->db->join('cabang c', 'p.id_cabang = c.id');
        // $this->db->like('category',$key);
        $product = $this->db->get('pelelangan p')->result();
    }

    public function produk_detail($id_barang)
    {
        $this->db->join('cabang a', 'a.id = b.id_cabang');
        $q =  $this->db->get_where('pelelangan b', ['b.id_barang' => $id_barang])->row();
        return $q;
    }

    public function bid()
    {
        $this->db->join('pelelangan', 'bid.id_barang = pelelangan.id_barang', 'left');
        $this->db->join('cabang', 'bid.id_cabang = cabang.id', 'left');
        $this->db->join('tbl_user', 'bid.id_user = tbl_user.user_id', 'left');
        $w = $this->db->get('bid');
        return $w->result();
    }

    public function getUserById($id_user)
    {
        return $this->db->get_where('tbl_user', ['user_id' => $id_user])->row();
    }

    public function getBarangById($id_barang)
    {
        return $this->db->get_where('pelelangan', ['id_barang' => $id_barang])->row();
    }

    public function getCabangById($id_cabang)
    {
        return $this->db->get_where('cabang', ['id' => $id_cabang])->row();
    }

    public function insert_bid($data3)
    {
        $this->db->insert('bid', $data3);
    }

    public function updateStatus($id)
    {
        $data = [
            'status' => 'pemenang'
        ];

        $this->db->where('id_lelang_bid', $id);
        return $this->db->update('bid', $data);
    }


    public function cabang()
    {

        $a = $this->db->get('cabang');
        return $a->result();
    }

    public function create_barang($data_produk)
    {
        $this->db->insert('pelelangan', $data_produk);
    }

    public function update_barang($id_barang, $data_produk)
    {
        $this->db->where('id_barang', $id_barang);
        return $this->db->update('pelelangan', $data_produk);
    }

    public function delete_barang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // public function datapenawar($id_barang)
    // {
    //     $this->db->join('tbl_user b', 'b.user_id = a.id_user');
    //     $this->db->join('pelelangan c', 'c.id_barang = a.id_barang');
    //     $this->db->join('cabang d', 'd.id = a.id_cabang');
    //     $data = $this->db->get_where('bid a', ['a.id_barang' => $id_barang])->result_array();
    //     $data = json_encode($data);
    //     echo $data;
    // }
}
