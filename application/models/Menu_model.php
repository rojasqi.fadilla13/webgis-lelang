<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT `user_submenu`.*,`user_menu`.`menu`
                FROM `user_submenu` JOIN `user_menu`
                ON `user_submenu`.`menu_id` = `user_menu`.`id`
        ";
        return $this->db->query($query)->result_array();
    }

    public function delete_submenu($table, $data)
    {
        $this->db->delete($table, $data);
    }

    public function edit_submenu($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('user_submenu', $data);
    }

    public function edit_menu($id)
    {
        $data = array(
            'menu' => $this->input->post('menu')
        );
        $this->db->where('id', $id);
        return $this->db->update('user_menu', $data);
    }

    public function delete_menu($table, $data)
    {
        $this->db->delete($table, $data);
    }
}
