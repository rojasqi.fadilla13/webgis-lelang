<?php
defined('BASEPATH') or exit('No direct script acces allowed');

class Gis_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('upload', 'session'));
    }

    public function inputcabang($object)
    {
        $this->db->insert('pemetaan', $object);
    }

    public function getcabang($param = 0)
    {
        return $this->db->get_where('pemetaan', array('id_cabang' => $param))->row();
    }

    public function updatecabang($param = 0)
    {
        $pemetaan = $this->getcabang($param);

        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_width']  = 1024 * 3;
        $config['max_height']  = 768 * 3;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
            $img_name = $pemetaan->image;
            $error = $this->upload->display_errors();
            print_r($error);
        } else {
            $photo = $this->upload->data();
            $img_name = $photo['file_name'];

            $object = array(
                'nama_cabang' => $this->input->post('nama_cabang'),
                'alamat' => $this->input->post('alamat'),
                'kota' => $this->input->post('kota'),
                'no_telp' => $this->input->post('no_telp'),
                'image' => $img_name,
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'deskripsi' => $this->input->post('deskripsi')
            );

            $this->db->update('pemetaan', $object, array('id_cabang' => $param));
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Cabang, Updated!</div>');
        }
    }

    public function cabang()
    {

        $a = $this->db->get('cabang');
        return $a->result();
    }

    public function deletecabang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function getAllData()
    {
        $this->db->select('*');
        $this->db->from('pemetaan');
        $query = $this->db->get();
        return $query->result();
    }

    public function datalokasi()
    {
        $this->db->select('*');
        $this->db->from('pemetaan');
        $query = $this->db->get();
        return $query->result();
    }
}
