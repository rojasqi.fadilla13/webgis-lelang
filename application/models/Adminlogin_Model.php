<?php

/**
 *
 */
class Adminlogin_Model extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }


  // LOGIN APP
  function login($user_name, $user_password)
  {
    //create query to connect user login database
    $this->db->select('*');
    $this->db->from('tbl_user');
    $this->db->where('user_name', $user_name);
    $this->db->where('user_password', MD5($user_password));
    $this->db->limit(1);

    //get query and processing
    $query = $this->db->get();
    if ($query->num_rows() == 1) {
      return $query->result(); //if data is true
    } else {
      return false; //if data is wrong
    }
  }
}
