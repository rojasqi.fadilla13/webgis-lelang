<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_login();
        $this->load->library(array('googlemaps'));
        $this->load->model('Gis_Model');
        $this->load->model('Model_produk');
    }
    public function index()
    {
        $data['title'] = 'My Profile';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('user/index', $data);
        $this->load->view('temanbody/footer');
    }

    public function edit()
    {
        $data['title'] = 'Edit Profile';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('user/edit', $data);
            $this->load->view('temanbody/footer');
        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            //cek jika ada gambar yg mau di upload
            $upload_image = $_FILES['image']['name'];
            // var_dump($upload_image);
            // die;
            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/images/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {
                    $old_image = $data['user']['image'];
                    if ($old_image != 'default.png') {
                        unlink(FCPATH . 'assets/images/' . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $this->db->set('user_name', $name);
            $this->db->where('user_email', $email);
            $this->db->update('tbl_user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Your profile has been, Updated!</div>');
            redirect('user');
        }
    }

    public function changePassword()
    {
        $data['title'] = 'Change Password';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->form_validation->set_rules('passwordsaatini', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('passwordbaru1', 'New Password', 'required|trim|min_length[8]|matches[passwordbaru2]');
        $this->form_validation->set_rules('passwordbaru2', 'Confirm New Password', 'required|trim|min_length[8]|matches[passwordbaru1]');

        if ($this->form_validation->run() == false) {
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('user/changepassword', $data);
            $this->load->view('temanbody/footer');
        } else {
            $passwordsaatini = $this->input->post('passwordsaatini');
            $password_baru = $this->input->post('passwordbaru1');
            if (!password_verify($passwordsaatini, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong, Current Password!</div>');
                redirect('user/changepassword');
            } else {
                if ($passwordsaatini == $password_baru) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">New password cannot be same with current password!</div>');
                    redirect('user/changepassword');
                } else {
                    // password benar
                    $password_hash = password_hash($password_baru, PASSWORD_DEFAULT);
                    $this->db->set('password', $password_hash);
                    $this->db->where('user_email', $this->session->userdata('user_email'));
                    $this->db->update('tbl_user');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Password, Changed!</div>');
                    redirect('user/changepassword');
                }
            }
        }
    }

    public function tampilanpemetaan()
    {
        $data['title'] = "Lokasi Cabang";
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->load->library('googlemaps');
        $config['center'] = '-6.9187126, 107.6148755';
        $config['zoom'] = '13';
        $this->googlemaps->initialize($config);

        $lokasi = $this->Gis_Model->datalokasi();
        foreach ($lokasi as $key => $value) :
            $marker = array();
            $marker['position'] = "$value->latitude, $value->longitude";

            $marker['animation'] = 'DROP';
            $marker['infowindow_content'] = '<div class="media" style="width:400px;">';
            $marker['infowindow_content'] .= '<div class="media-left">';
            $marker['infowindow_content'] .= '<img src="' . base_url("assets/images/{$value->image}") . '" class="media-object" style="width:150px">';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '<div class="media-body">';
            $marker['infowindow_content'] .= '<h5 class="media-heading">' . $value->nama_cabang . '</h5>';
            $marker['infowindow_content'] .= '<p>No Telp : <strong>' . $value->no_telp . '</strong></p>';
            $marker['infowindow_content'] .= '<p>' . $value->alamat . '</p>';
            $marker['infowindow_content'] .= '<p>' . $value->deskripsi . '</p>';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '</div>';
            $marker['icon'] = base_url("assets/icon/pgd.png");
            $this->googlemaps->add_marker($marker);
        endforeach;

        $this->googlemaps->initialize($config);
        $data['map'] = $this->googlemaps->create_map();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('user/lokasi', $data);
        $this->load->view('temanbody/footer');
    }

    // public function searchQuery()
    // {
    // 	$this->db->select('pemetaan.*, categories.name as category');

    // 	$this->db->join('hotelCategories', 'hotel.ID = hotelCategories.category_id', 'left');

    // 	$this->db->join('categories', 'hotelCategories.category_id = categories.category_id', 'left');

    // 	switch ($this->input->get('price')) 
    // 	{
    // 		case '<100K':
    // 			$this->db->where('hotel.price <', 100000);
    // 			break;
    // 		case '100K-300K':
    // 			$this->db->where('hotel.price >=', 100000);
    // 			$this->db->where('hotel.price <=', 300000);
    // 			break;
    // 		case '300K-500K':
    // 			$this->db->where('hotel.price >=', 300000);
    // 			$this->db->where('hotel.price <=', 500000);
    // 			break;
    // 		case '500K':
    // 			$this->db->where('hotel.price >=', 500000);
    // 			break;
    // 	}

    public function lelang($id = 0)
    {
        $data['title'] = "Lelang";
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();


        // $key = $this->input->post('search');

        if ($id == 0) {
            $data['produk'] = $this->Model_produk->all();
            $data['cabang'] = $this->Model_produk->cabang();
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('menu/v_lelang', $data);
            $this->load->view('temanbody/footer');
        } else {
            $data['produk'] = $this->Model_produk->allcabang($id);
            $data['cabang'] = $this->Model_produk->cabang();
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('menu/v_lelang', $data);
            $this->load->view('temanbody/footer');
        }

        // } elseif ($key != '') {
        //     $data['produk'] = $this->Model_produk->allspec($key);
        //     $data['cabang'] = $this->Model_produk->cabang();
        //     $this->load->view('temanbody/header', $data);
        //     $this->load->view('temanbody/topbar', $data);
        //     $this->load->view('temanbody/sidebar', $data);
        //     $this->load->view('menu/v_lelang', $data);
        //     $this->load->view('temanbody/footer');
        // }
    }

    function produk_detail($id_barang)
    {
        $data['title'] = "Lelang";
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();
        // $data1['id_barang'] = $this->input->post('id_barang');
        // $data2['id_cabang'] = $this->input->post('id_cabang');

        // $id_barang = array(
        //     // 'id_barang' => $this->input->post('id_barang'),
        //     'id_cabang' => $this->input->post('id_cabang')
        // );
        $data['barang'] = $this->Model_produk->produk_detail($id_barang);

        $data['detail'] = $this->Model_produk->bid();

        // $data3 = array(
        //     'id_barang'     => $this->input->post('id_barang'),
        //     'id_cabang'     => $this->input->post('id_cabang'),
        //     'id_user'     => $this->input->post('id_user'),
        //     'harga_lelang'    => $this->input->post('harga_lelang'),
        //     'status'       => $this->input->post('status'),
        //     'tanggal'      => time()
        // );
        // $this->Model_produk->insert_bid($data3);

        // $data['related'] = $this->Model_produk->pilih_cabang($where);
        // $data['comment'] = $this->Model_produk->komen($where);
        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_lelang_detail', $data);
        $this->load->view('temanbody/footer');
    }

    public function bid()
    {
        $data['title'] = "Lelang";
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $data['detail'] = $this->Model_produk->bid();

        $data = array(
            'id_barang'     => $this->input->post('id_barang'),
            'id_cabang'     => $this->input->post('id_cabang'),
            'id_user'     => $this->input->post('id_user'),
            'harga_lelang'    => $this->input->post('harga_lelang'),
            'status'       => $this->input->post('status'),
            'tanggal'      => time()
        );
        $this->Model_produk->insert_bid($data);

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_lelang_detail', $data);
        $this->load->view('temanbody/footer');
    }
}
