<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_login();
        $this->load->model('Menu_model');
    }
    public function index()
    {
        $data['title'] = 'Management Menu';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('menu', 'Menu', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('temanbody/footer');
        } else {
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu Added!</div>');
            redirect('menu');
        }
    }


    public function submenu()
    {
        $data['title'] = 'Management SubMenu';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->load->model('Menu_model', 'menu');
        $data['subMenu'] = $this->menu->getSubMenu();
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('temanbody/header', $data);
            $this->load->view('temanbody/topbar', $data);
            $this->load->view('temanbody/sidebar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('temanbody/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active')
            ];
            $this->db->insert('user_submenu', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">SubMenu Added!</div>');
            redirect('menu/submenu');
        }
    }

    function delete_submenu()
    {
        $id['id'] = $this->uri->segment(3);
        $this->Menu_model->delete_submenu('user_submenu', $id);
        redirect('menu/submenu');
    }

    function edit_submenu()
    {
        $id = $this->input->post('id');
        $data = [
            'title' => $this->input->post('title'),
            'menu_id' => $this->input->post('menu_id'),
            'url' => $this->input->post('url'),
            'icon' => $this->input->post('icon'),
            'is_active' => $this->input->post('is_active')
        ];

        $this->Menu_model->edit_submenu($id, $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">SubMenu Updated!</div>');
        redirect('menu/submenu');
    }

    function edit_menu()
    {
        $id = $this->input->post('id');
        $this->Menu_model->edit_menu($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu Updated!</div>');
        redirect('menu');
    }

    function delete_menu()
    {
        $id['id'] = $this->uri->segment(3);
        $this->Menu_model->delete_menu('user_menu', $id);
        redirect('menu');
    }
}
