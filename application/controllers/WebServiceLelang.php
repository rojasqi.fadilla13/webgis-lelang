<?php
class WebServiceLelang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDataLelang($id_barang)
    {
        $this->db->join('tbl_user b', 'b.user_id = a.id_user');
        $this->db->join('pelelangan c', 'c.id_barang = a.id_barang');
        $this->db->join('cabang d', 'd.id = a.id_cabang');
        $data = $this->db->get_where('bid a', ['a.id_barang' => $id_barang])->result_array();
        $data = json_encode($data);
        echo $data;
    }

    public function submitLelang()
    {
        $data = [
            'id_barang' => $this->input->post('id_barang'),
            'id_cabang' => $this->input->post('id_cabang'),
            'id_user' => $this->session->userdata('id_user'),
            'harga_lelang' => $this->input->post('harga_lelang'),
            'tanggal' => date('Y-m-d h:i:s'),
        ];

        $this->db->insert('bid', $data);

        $return = [
            'status' => true,
            'code' => 201
        ];

        $return = json_encode($return);

        echo $return;
    }
}
