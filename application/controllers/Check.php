<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Check extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('user_email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {

            $data['title'] = 'Pegadaian Lelang Login Page';
            $this->load->view('temanbody/check_header', $data);
            $this->load->view('check/login');
            $this->load->view('temanbody/check_footer');
        } else {
            // kalo sukses
            $this->_login();
        }
    }


    private function _login()
    {
        $user_email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('tbl_user', ['user_email' => $user_email])->row_array();

        // jika usernya ada
        if ($user) {
            // jika usernya aktif
            if ($user['is_active'] == 1) {
                // cek pas
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'user_email' => $user['user_email'],
                        'role_id' => $user['role_id'],
                        'id_user' => $user['user_id']
                    ];
                    $this->session->set_userdata($data);
                    if ($user['role_id'] == 1) {
                        redirect('admin');
                    } else
                        redirect('user');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Your password is wrong!</div>');
                    redirect('check');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is has not been Activated!</div>');
                redirect('check');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered!</div>');
            redirect('check');
        }
    }
    // var_dump($user);
    // die;

    public function register()
    {
        if ($this->session->userdata('user_email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.user_email]', [
            'is_unique' => 'Email tos aya nungagunakeun bray!',
        ]);
        $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]', [
            'matches' => 'Password te cocok bray!',
            'min_length' => 'Password pendek teing!'

        ]);
        $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Pegadaian Lelang Registration';
            $this->load->view('temanbody/check_header', $data);
            $this->load->view('check/register');
            $this->load->view('temanbody/check_footer');
        } else {
            $email = $this->input->post('email', true);
            $data = [
                'user_name' => htmlspecialchars($this->input->post('name', true)),
                'user_email' => htmlspecialchars($email),
                'image' => 'default.png',
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'is_active' => 0,
                'date_created' => time()
            ];

            //token
            $token = base64_encode(random_bytes(32));
            $user_token = [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];
            // var_dump($token);
            // die;

            $this->db->insert('tbl_user', $data);
            $this->db->insert('tbl_user_token', $user_token);

            $this->_sendEmail($token, 'verify');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Thank you, your account has been created. Please, Actived Account!</div>');
            redirect('check');
        }
    }

    private function _sendEmail($token, $type)
    {

        $this->load->library('email');
        $this->email->from('userpsd.0001@gmail.com', 'Pegadaian Lelang WebGIS');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {

            // print_r('coco');
            // die;
            $this->email->subject('Account Verification Pegadaian Lelang WebGIS');
            $this->email->message('Click this link to verify your account : 
                                <a href="' . base_url() . 'check/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Aktifkeun</a>');
            $em = $this->email->send();
            // print_r($em);
            // die;
        }

        // if ($this->email->send()) {
        //     return true;
        // } else {
        //     echo $this->email->print_debugger();
        //     die;
        // }
    }


    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('tbl_user', ['user_email' => $email])->row_array();

        if ($user) {
            $user_token = $this->db->get_where('tbl_user_token', ['token' => $token])->row_array();
            if ($user_token) {
                if (time() - $user_token['date_created'] < (86400)) {
                    $this->db->set('is_active', 1);
                    $this->db->where('user_email', $email);
                    $this->db->update('tbl_user');

                    $this->db->delete('tbl_user_token', ['email' => $email]);

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">' . $email . ' has been activated, Please Login!</div>');
                    redirect('check');
                } else {

                    $this->db->delete('tbl_user', ['user_email' => $email]);
                    $this->db->delete('tbl_user_token', ['email' => $email]);


                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Activation account failed! cause, Token expired!</div>');
                    redirect('check');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Activation account failed! cause, Invalid token!</div>');
                redirect('check');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Activation account failed! cause, Invalid email!</div>');
            redirect('check');
        }
    }


    public function logout()
    {
        $this->session->unset_userdata('user_email');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">You have been logout, Thankyou!</div>');
        redirect('check');
    }

    public function block()
    {
        $this->load->view('check/block');
    }
}
