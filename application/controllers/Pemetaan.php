<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemetaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_login();
        $this->load->library(array('googlemaps'));
        $this->load->model('Gis_Model');
    }

    public function index()
    {
        $data['title'] = 'Management Pemetaan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();
        $data['cabang'] = $this->Gis_Model->cabang();

        // $data['pemetaan'] = $this->Gis_Model->all();

        $this->load->library('googlemaps');
        $config['center'] = '-6.9032739, 107.5731165';
        $config['zoom'] = '15';
        $this->googlemaps->initialize($config);
        $marker['position'] = '-6.9032739, 107.5731165';
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setMapToForm(event.latLng.lat(),event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_pemetaan', $data);
        $this->load->view('temanbody/footer');
    }

    public function inputcabang()
    {
        $data['title'] = 'Management Pemetaan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();
        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_width']  = 1024 * 3;
        $config['max_height']  = 768 * 3;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            print_r($error);
        } else {
            $photo = $this->upload->data();
            $img_name = $photo['file_name'];

            $object = array(
                'nama_cabang' => $this->input->post('nama_cabang'),
                'alamat' => $this->input->post('alamat'),
                'kota' => $this->input->post('kota'),
                'no_telp' => $this->input->post('no_telp'),
                'image' => $img_name,
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'deskripsi' => $this->input->post('deskripsi')
            );

            $this->Gis_Model->inputcabang($object);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Cabang, Added!</div>');
            redirect('pemetaan');
        }
    }

    public function updatecabang($param = 0)
    {
        $data['title'] = 'Data Pemetaan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $this->form_validation->set_rules('nama_cabang', 'Nama Cabang', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('kota', 'Kota', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $this->Gis_Model->updatecabang($param);

            redirect('pemetaan/datapemetaan');
        }

        $data['pemetaan'] = $this->Gis_Model->getcabang($param);
        $data['cabang'] = $this->Gis_Model->cabang();


        $config['map_div_id'] = "map-add";
        $config['map_height'] = "250px";
        $config['center'] = $data['pemetaan']->latitude . ',' . $data['pemetaan']->longitude;
        $config['zoom'] = '15';
        $config['map_height'] = '300px;';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = $data['pemetaan']->latitude . ',' . $data['pemetaan']->longitude;
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setMapToForm(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_updatepemetaan', $data);
        $this->load->view('temanbody/footer');
    }

    public function datapemetaan()
    {
        $data['title'] = 'Data Pemetaan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $data['datapemetaan'] = $this->db->get('pemetaan')->result_array();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_datapemetaan', $data);
        $this->load->view('temanbody/footer');
    }

    public function deletecabang($id_cabang)
    {
        if (empty($id_cabang)) {
            redirect('pemetaan');
        } else {
            $where = array('id_cabang' => $id_cabang);
            $this->Gis_Model->deletecabang($where, 'pemetaan');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Cabang Deleted!</div>');
            redirect('pemetaan/datapemetaan');
        }
    }
}
