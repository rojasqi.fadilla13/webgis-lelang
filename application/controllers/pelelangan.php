<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelelangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_login();
        $this->load->model('Model_produk');
    }

    public function index()
    {
        $data['title'] = 'Data Pelelangan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $data['produk'] = $this->Model_produk->all();
        $data['cabang'] = $this->Model_produk->cabang();

        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/v_pelelangan', $data);
        $this->load->view('temanbody/footer');
    }

    public function create_barang()
    {
        $data['title'] = 'Data Pelelangan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();
        $data['produk'] = $this->Model_produk->all();

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('gambar')) {
            $error = $this->upload->display_errors();
            print_r($error);
        } else {
            $result = $this->upload->data();
            $img_name = $result['file_name'];

            $data_product = array(
                'id_barang'     => $this->input->post('id_barang'),
                'nama_barang'     => $this->input->post('nama_barang'),
                'harga_barang'     => $this->input->post('harga_barang'),
                'tgl_buka'      => time(),
                'id_cabang'    => $this->input->post('id_cabang'),
                'deskripsi'       => $this->input->post('deskripsi'),
                'gambar'         => $img_name
            );

            $this->Model_produk->create_barang($data_product);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Barang Added!</div>');
            redirect('pelelangan');
        }
    }

    public function update_barang()
    {
        $data['title'] = 'Data Pelelangan';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();


        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';


        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('gambar')) {
            $id_barang = $this->input->post('id_barang');
            $data_produk = array(
                'gambar' => $this->input->post('gambar2'),
                'nama_barang'     => $this->input->post('nama_barang'),
                'harga_barang'     => $this->input->post('harga_barang'),
                'tgl_buka'      => time(),
                'id_cabang'    => $this->input->post('id_cabang'),
                'deskripsi'       => $this->input->post('deskripsi'),
            );
            $this->Model_produk->update_barang($id_barang, $data_produk);
        } else {
            $id_barang = $this->input->post('id_barang');
            $gambar2 = $this->upload->data();

            $data_produk = array(
                'gambar' => $gambar2,
                'nama_barang'     => $this->input->post('nama_barang'),
                'harga_barang'     => $this->input->post('harga_barang'),
                'tgl_buka'      => time(),
                'id_cabang'    => $this->input->post('id_cabang'),
                'deskripsi'       => $this->input->post('deskripsi'),
            );
            $this->Model_produk->update_barang($id_barang, $data_produk);
        }
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Barang Updated!</div>');
        redirect('pelelangan');
    }

    public function delete_barang($id_barang)
    {
        if (empty($id_barang)) {
            redirect('pelelangan');
        } else {
            $where = array('id_barang' => $id_barang);
            $this->Model_produk->delete_barang($where, 'pelelangan');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Barang Deleted!</div>');
            redirect('pelelangan');
        }
    }


    public function datapenawar()
    {
        $data['title'] = 'Data Penawar';
        $data['user'] = $this->db->get_where('tbl_user', ['user_email' =>
        $this->session->userdata('user_email')])->row_array();

        $data['penawar'] = $this->Model_produk->bid();
        // print_r($data['penawar']);
        // die;
        $this->load->view('temanbody/header', $data);
        $this->load->view('temanbody/topbar', $data);
        $this->load->view('temanbody/sidebar', $data);
        $this->load->view('menu/datapenawar', $data);
        $this->load->view('temanbody/footer');
    }

    public function updateStatus($id, $id_user, $id_barang, $id_cabang)
    {
        $this->Model_produk->updateStatus($id);
        $user = $this->Model_produk->getUserById($id_user);
        $id_barang = $this->Model_produk->getBarangById($id_barang);
        $id_cabang = $this->Model_produk->getCabangById($id_cabang);

        $mess = "Selamat <b>$user->user_name</b>, Anda terpilih sebagai pemenang lelang dengan nama barang <b>$id_barang->nama_barang</b>, Silahkan segera mendatangi cabang <b>$id_cabang->cabang</b> untuk tanda tangan dan pengambilan barang, Jika lebih dari 1x24 Jam maka dianggap Batal, Terimakasih.";
        $this->load->library('email');
        $this->email->from('userpsd.0001@gmail.com', 'Pemenang Lelang');
        $this->email->to($user->user_email);
        $this->email->subject('Notifikasi Pemenang Lelang');
        $this->email->message($mess);
        $send = $this->email->send();

        redirect('pelelangan/datapenawar');
    }
}
