<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">User</a>
            </li>
            <li class="breadcrumb-item active">Change Password</li>
        </ol>

        <!-- Page Content -->
        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
                <form action="<?= base_url('user/changepassword') ?>" method="post">
                    <div class="form-group">
                        <label for="passwordsaatini">Current Password</label>
                        <input type="password" class="form-control" id="passwordsaatini" name="passwordsaatini">
                        <?php echo form_error('passwordsaatini', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="passwordbaru1">New Password</label>
                        <input type="password" class="form-control" id="passwordbaru1" name="passwordbaru1">
                        <?php echo form_error('passwordbaru1', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <label for="passwordbaru2">Repeat Password</label>
                        <input type="password" class="form-control" id="passwordbaru2" name="passwordbaru2">
                        <?php echo form_error('passwordbaru2', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Change Password</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /.container-fluid -->