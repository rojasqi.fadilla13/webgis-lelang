<div id="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">User</a>
            </li>
            <li class="breadcrumb-item active">Lokasi Cabang</li>
        </ol>

        <div class="card bg-light mb-10" style="max-width: 80rem;">
            <div class="card-header">Lokasi Pemetaan Cabang Bandung 2</div>
            <div class="card-body">
                <?php echo $map['html']; ?>
                <?php echo $map['js']; ?>
            </div>
        </div>


    </div>
</div>