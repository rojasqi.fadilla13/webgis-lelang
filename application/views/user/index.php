<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">User</a>
            </li>

            <li class="breadcrumb-item active">My Profile</li>
        </ol>

        <div class="row">
            <div class="col-lg-5">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>

        <!-- Page Content -->

        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-header">
                    My Profile</div>
                <div class="card-body">
                    <img class="card-img" src="<?= base_url('assets/images/default1.png') ?>">
                    <canvas id="myPieChart" width="15%" height="15"></canvas>
                    <h5 class="card-title"><?= $user['user_name']; ?></h5>
                    <p class="card-text"><?= $user['user_email']; ?></p>
                </div>
                <div class="card-footer small text-muted"><small class="text-muted">Member Since, <?= date('d F Y', $user['date_created']) ?></small></div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->