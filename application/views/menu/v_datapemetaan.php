<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Pemetaan</a>
            </li>
            <li class="breadcrumb-item active">Data Pemetaan</li>
        </ol>

        <?= $this->session->flashdata('message'); ?>

        <table class="display nowrap" id="example" style="width:100%">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Cabang</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Kota</th>
                    <th scope="col">No Telp</th>
                    <th scope="col">Image</th>
                    <th scope="col">Latitude</th>
                    <th scope="col">Longitude</th>
                    <th scope="col">Deskripsi</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $q = 1; ?>
                <?php foreach ($datapemetaan as $sm) : ?>
                    <tr>
                        <th scope="row"><?= $q; ?></th>
                        <td><?= $sm['nama_cabang']; ?></td>
                        <td><?= $sm['alamat']; ?></td>
                        <td><?= $sm['kota']; ?></td>
                        <td><?= $sm['no_telp']; ?></td>
                        <td><?= $sm['image']; ?></td>
                        <td><?= $sm['latitude']; ?></td>
                        <td><?= $sm['longitude']; ?></td>
                        <td><?= $sm['deskripsi']; ?></td>
                        <td>
                            <a href="<?= base_url('pemetaan/updatecabang/' . $sm['id_cabang']); ?>" class="badge badge-warning">Edit</a>
                            <a href="<?= base_url('pemetaan/deletecabang/' . $sm['id_cabang']); ?>" onclick="return validasi();" class="badge badge-danger">Delete</a>
                        </td>
                    </tr>
                    <?php $q++; ?>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>