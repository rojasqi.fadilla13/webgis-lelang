<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Admin</a>
            </li>
            <li class="breadcrumb-item active">Management Menu</li>
        </ol>

        <!-- Page Content -->
        <div class="row">
            <div class="col-lg-6">
                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <?= $this->session->flashdata('message'); ?>

                <a href="" class="btn btn-success mb-3" data-toggle="modal" data-target="#addMenu">Add Menu</a>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Menu</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $q = 1; ?>
                        <?php foreach ($menu as $m) : ?>
                            <tr>
                                <th scope="row"><?= $q; ?></th>
                                <td><?= $m['menu']; ?></td>
                                <td>
                                    <a class="badge badge-warning" data-toggle="modal" href="" data-target="#modalEdit<?= $m['id']; ?>">Edit</a>
                                    <a class="badge badge-danger" href="<?= base_url('menu/delete_menu/' . $m['id']); ?>" onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                            <?php $q++; ?>

                            <!-- Modal EDIT  -->
                            <div id="modalEdit<?= $m['id']; ?>" tabindex="-1" class="modal fade">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalEdit">Edit Menu</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="<?= base_url('menu/edit_menu'); ?>" method="post" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="hidden" class="form-control" id="id" name="id" value="<?= $m['id']; ?>">
                                                    <input type="text" class="form-control" id="menu" name="menu" value="<?= $m['menu']; ?>">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </tbody>
                <?php endforeach; ?>
                </table>

            </div>
        </div>

    </div>
</div>

<!-- Modals ADD -->
<div class="modal fade" id="addMenu" tabindex="-1" role="dialog" aria-labelledby="addMenuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMenuLabel">Add New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('menu'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Type new menu">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- <script>
    //     $(document).ready(function() {
    //         // Untuk sunting
    //         $('#modalEdit').on('show.bs.modal', function(event) {
    //             var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
    //             var modal = $(this)

    //             // Isi nilai pada field
    //             modal.find('#id').attr("value", div.data('id'));
    //             modal.find('#menu').attr("value", div.data('menu'));
    //         });
    //     });
    // 
</script> -->