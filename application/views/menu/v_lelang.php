<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">User</a>
            </li>
            <li class="breadcrumb-item active">Lelang</li>
        </ol>

        <center>
            <div class="btn-group">
                <button type="button" class="btn btn-danger">Pilih Cabang</button>
                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('user/lelang/'); ?>">All</a>
                    <?php foreach ($cabang as $key) { ?>
                        <a class="dropdown-item" href="<?= base_url('user/lelang/' . $key->id); ?>"><?= $key->cabang; ?></a>
                    <?php } ?>
                </div>
            </div>
        </center>
        <br>

        <div class="row">
            <?php foreach ($produk as $key) { ?>
                <div class="col-lg-4">
                    <div class="card mb-3">
                        <div class="card-header">
                            Cabang Pembuka :
                            <span class="badge badge-primary"><?php echo $key['cabang']; ?> </span></div>
                        <div class="card-body">
                            <img class="rounded" src="<?php echo base_url() . '/assets/images/' . $key['gambar']; ?>" style="height:300px;width:300px" alt="product-img" />
                            <a href="<?php echo base_url('user/produk_detail/' . $key['id_barang']); ?>">&nbsp;
                                <center><button class="btn btn-danger btn-sm" type="button">Detail Barang</button></center>
                            </a>
                        </div>
                        <div class="card-footer small text-muted">
                            <h5 style="color: #0078FF;"><?php echo $key['nama_barang'] ?></h5>
                            <p class="harga_barang"><b><?= 'Rp. ' . number_format($key['harga_barang'], 0, ',', '.'); ?></b></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</div>