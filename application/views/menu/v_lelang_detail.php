<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">User</a>
            </li>
            <li class="breadcrumb-item active">Lelang</li>
        </ol>

        <div id="suksesAlert" class="alert alert-success" role="alert" style="display:none">
            Penawaran berhasil diinput!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div id="gagalAlert" class="alert alert-danger" role="alert" style="display:none">
            Penawaran gagal diinput!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="row mt-20">
            <div class="col-md-5">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">

                            <img class="d-block w-100 rounded" src="<?php echo base_url() . '/assets/images/' . $barang->gambar; ?>" alt="First slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <h2><?= $barang->nama_barang; ?></h2>
                <p><?= $barang->deskripsi; ?></p>
                <div class="form-group">
                    <!-- <blockquote class="blockquote"> -->
                    <a><a>Harga : </a>
                        <h3>Rp. <?= number_format($barang->harga_barang); ?></h3>
                    </a><br>
                    <p><a>Cabang Pembuka : </a><span class="badge badge-danger"><?= $barang->cabang ?></span></p>
                    <!-- </blockquote> -->
                </div>
                <div class="form-group">
                    <a href="" data-target="#bid" data-toggle="modal" method="post">
                        <input type="submit" class="btn btn-warning" value="Bid Now" />
                    </a>
                </div>
            </div>

            <div class="col-sm-12"><br>&nbsp;
                <h4>Data Penawar</h4>
                <table class="table table-hover" id="tblelang">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Penawar</th>
                            <th>Cabang</th>
                            <th>Email Penawar</th>
                            <th>Tanggal</th>
                            <th>Jumlah Penawaran</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="table-row">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modals ADD -->


<div id="bid" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addMenuLabel">Masukan Harga Penawaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="number" class="form-control" id="harga_lelang" name="harga_lelang" placeholder="ex.5.000.000">
                        <input type="hidden" id="id_barang" value="<?php echo $barang->id_barang ?>" />
                        <input type="hidden" id="id_cabang" value="<?php echo $barang->id_cabang ?>" />
                        <!-- <input type="hidden" id="id_user" value="<?php echo $this->session->userdata('id_user'); ?>" /> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="btn_add" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js">
</script>

<script>
    $(document).ready(function() {
        $('#suksesAlert').hide()
        $('#gagalAlert').hide()

        getData()

        function getData() {
            var id_barang = $("#id_barang").val()
            $.get("http://localhost/lelang-pgd/index.php/WebServiceLelang/getDataLelang/" + id_barang, function(data, status) {
                if (status) {
                    $('#table-row').html("")
                    let no = 0;
                    var dataFix = JSON.parse(data)
                    // console.log(dataFix)
                    dataFix.forEach(element => {
                        console.log(dataFix)
                        var number_string = element.harga_lelang.toString(),
                            sisa = number_string.length % 3,
                            harga_lelang = number_string.substr(0, sisa),
                            ribuan = number_string.substr(sisa).match(/\d{3}/g);

                        if (ribuan) {
                            separator = sisa ? '.' : '';
                            harga_lelang += separator + ribuan.join('.');
                        }
                        // var tanggal = moment(element.tanggal).format('MMMM Do YYYY, h:mm:ss a');
                        var t = $('#tblelang').DataTable();
                        no++;
                        t.row.add([
                            "<center>" + no + "</center>",
                            "<center>" + element.user_name + "</center>",
                            "<center>" + element.cabang + "</center>",
                            "<center>" + element.user_email + "</center>",
                            "<center>" + moment(element.tanggal).format('MMMM Do YYYY, h:mm:ss a') + "</center>",
                            "<center>" + 'Rp.' + harga_lelang + "</center>",
                            "<center>" + element.status + "</center>",
                        ]).draw(false);
                    });
                } else {
                    alert('Load Data Failed')
                }
            });
        }

        function clearTable() {
            var table = $('#tblelang').DataTable();
            table
                .clear()
                .draw();
        }

        function clearModalAdd() {
            $('#harga_lelang').val('')
        }

        $(document).on('click', '#btn_add', function() {

            var id_barang = $('#id_barang').val()
            var harga_lelang = $('#harga_lelang').val()
            var id_cabang = $('#id_cabang').val()

            $.post("http://localhost/lelang-pgd/index.php/WebServiceLelang/submitLelang", {
                    id_barang: id_barang,
                    id_cabang: id_cabang,
                    harga_lelang: harga_lelang,
                },
                function(data) {
                    var resp = JSON.parse(data);
                    if (resp.status == true) {
                        clearTable()
                        $('#bid').modal('hide')
                        clearModalAdd()
                        getData()
                        $('#suksesAlert').show()
                        // toastr.success('Penawaran berhasil diinput!')
                    } else {
                        // toastr.error('Penawaran gagal diinput!')
                        $('#gagalAlert').show()
                    }
                });
        })
    })
</script>