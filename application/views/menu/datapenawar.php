<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Pelelangan</a>
            </li>
            <li class="breadcrumb-item active">Data Penawar</li>
        </ol>
        <!-- Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <?= $this->session->flashdata('message'); ?>
                <table class="table table-hover col-sm-12" id="example">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga Barang</th>
                            <th scope="col">Jumlah Penawar</th>
                            <th scope="col">Cabang</th>
                            <th scope="col">Email Penawar</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $q = 1; ?>
                        <?php foreach ($penawar as $m) : ?>
                            <tr>
                                <th scope="row"><?= $q; ?></th>
                                <td><?= $m->nama_barang; ?></td>
                                <td><?= $m->harga_barang; ?></td>
                                <td><?= $m->harga_lelang; ?></td>
                                <td><?= $m->cabang; ?></td>
                                <td><?= $m->user_email;; ?></td>
                                <td><?= $m->tanggal; ?></td>
                                <td>
                                    <a class="badge badge-warning" id="status" href="<?= base_url(); ?>index.php/pelelangan/updateStatus/<?= $m->id_lelang_bid; ?>/<?= $m->id_user; ?>/<?= $m->id_barang; ?>/<?= $m->id_cabang; ?>"><?= $m->status; ?></a>
                                    <a class="badge badge-danger" href="" onclick="return confirm('Are you sure?')">Delete</a>
                                </td>
                            </tr>
                            <?php $q++; ?>
                    </tbody>
                <?php endforeach; ?>
                </table>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click', '#status', function() {
            console.log('aw')
        })

    })
</script>