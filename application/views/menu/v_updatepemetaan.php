<div id="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Pemetaan</a>
            </li>
            <li class="breadcrumb-item active">Update Management Pemetaan</li>
        </ol>
        <?php echo $this->session->flashdata('message'); ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="container">
                <div class="card bg-light mb-6" style="max-width: 80rem;">
                    <div class="card-header">Form Update Pemetaan Cabang Bandung 2
                    </div>
                    <div class="card-body">
                        <form action="<?= current_url(); ?>" method="post" enctype="multipart/form-data">
                            <form class="form-horizontal">
                                <input type="hidden" class="form-control" id="id_cabang" name="id_cabang" value="<?= $pemetaan->id_cabang; ?>">
                                <div class="form-group">
                                    <label>Cabang Pembuka</label>
                                    <select name="id_cabang" class="form-control" required>
                                        <option selected value="<?= $pemetaan->id_cabang; ?>" required><?= $pemetaan->nama_cabang; ?></option>
                                        <?php foreach ($cabang as $c) { ?>
                                            <option value="<?= $c->id ?>"><?= $c->cabang ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat :</label>
                                    <div class="form-group">
                                        <textarea name="alamat" class="form-control" rows="3"><?= $pemetaan->alamat; ?></textarea>
                                        <p class="help-block"><?php echo form_error('alamat', '<small class="text-red">', '</small>'); ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="kota">Kota</label>
                                    <input type="text" class="form-control" id="kota" name="kota" value="<?= $pemetaan->kota; ?>">
                                    <?php echo form_error('kota', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="no_telp">No Telp</label>
                                    <input type="text" class="form-control" id="no_telp" name="no_telp" value="<?= $pemetaan->no_telp; ?>">
                                    <?php echo form_error('no_telp', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label><br>
                                    <div class="custom-file col-sm-4">
                                        <input type="file" class="custom-file-input" id="image" name="image">
                                        <label class="custom-file-label" for="image">Choose file</label>
                                    </div>
                                    <div class="custom-file col-md-4">

                                        <?php if ($pemetaan->image != '') : ?>
                                            <img src="<?= base_url("assets/images/{$pemetaan->image}") ?>" height="150" class="rounded">
                                        <?php endif; ?>
                                    </div>
                                </div><br>
                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi :</label>
                                    <div class="form-group">
                                        <textarea name="deskripsi" class="form-control" rows="6"><?= $pemetaan->deskripsi; ?></textarea>
                                        <p class="help-block"><?php echo form_error('deskripsi', '<small class="text-red">', '</small>'); ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="koordinat">Koordinat :</label>
                                    <label class="sr-only" for="latitude">Latitude</label>
                                    <div class="input-group mb-2 mr-sm-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-map-marker-alt"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="latitude" name="latitude" value="<?= $pemetaan->latitude; ?>">
                                        <p class="help-block"><?php echo form_error('latitude', '<small class="text-red">', '</small>'); ?></p>
                                    </div>
                                    <label class="sr-only" for="longitude">Longitude</label>
                                    <div class="input-group mb-2 mr-sm-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-map-marker-alt"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="longitude" name="longitude" value="<?= $pemetaan->longitude; ?>">
                                        <p class="help-block"><?php echo form_error('longitude', '<small class="text-red">', '</small>'); ?></p>
                                    </div>
                                    <div class="col-md-offset-2">
                                        <?php echo $map['html'] ?>
                                        <?php echo $map['js'] ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success mb-3"><i class="fa fa-save"></i> Save</button>
                                    <button class="btn btn-warning mb-3" onClick="refreshPage()"><i class="fa fa-trash-restore-alt"></i> Reset</button>
                                </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<br>