<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="">Pelelangan</a>
            </li>
            <li class="breadcrumb-item active">Data Pelelangan</li>
        </ol>

        <?= $this->session->flashdata('message'); ?>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-20">
                <div class="container">
                    <table id="myTable" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="2%">No</th>
                                <th width="20%">Nama Barang</th>
                                <th width="8%">Harga</th>
                                <th width="15%">Tanggal Pembuka</th>
                                <th width="10%">Cabang Pembuka</th>
                                <th width="10%">Deskripsi</th>
                                <th width="15%">Gambar</th>
                                <th width="15%">
                                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addbarang"><i class="fas fa-fw fa-plus-square" style="text-align: center;"></i> Tambah</button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $q = 1; ?>
                            <?php foreach ($produk as $barang) : ?>
                                <tr>
                                    <td><?= $q; ?></td>
                                    <td><?= $barang['nama_barang']; ?></td>
                                    <td><?= number_format($barang['harga_barang'], 0, ',', '.'); ?></td>
                                    <td><?= date('d F Y', $barang['tgl_buka']) ?></td>
                                    <td><?= $barang['cabang']; ?></td>
                                    <td><textarea readonly style="width:300px; height:120px;"><?= $barang['deskripsi']; ?></textarea></td>
                                    <td><img style="width:100px; height:130px;" src="<?= base_url() . 'assets/images/' . $barang['gambar']; ?>"></td>
                                    <td>
                                        <a class="badge badge-warning" data-toggle="modal" data-target="#barangedit<?= $barang['id_barang']; ?>">Edit</a>
                                        <a class="badge badge-danger" href="<?= base_url('pelelangan/delete_barang/' . $barang['id_barang']); ?>" onclick="return validasi();">Delete</a>
                                    </td>
                                </tr>
                                <?php $q++; ?>

                                <!-- Modal Update-->
                                <div id="barangedit<?= $barang['id_barang']; ?>" tabindex="-1" class="modal fade">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Update Barang</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?= base_url('pelelangan/update_barang'); ?>" method="post" enctype="multipart/form-data">
                                                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="id_barang" value="<?= $barang['id_barang']; ?>">
                                                    <input style="font-size: 15px" style="font-size: 15px" type="hidden" class="form-control" name="gambar2" value="<?= $barang['gambar']; ?>">
                                                    <div class="form-group">
                                                        <label>Nama Barang</label>
                                                        <input type="text" class="form-control" name="nama_barang" value="<?= $barang['nama_barang']; ?>" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Harga Barang</label>
                                                        <input type="text" class="form-control" name="harga_barang" value="<?= $barang['harga_barang']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Cabang Pembuka</label>
                                                        <select name="id_cabang" class="form-control" required>
                                                            <option selected value="<?= $barang['id_cabang'] ?>" required><?= $barang['cabang']; ?></option>
                                                            <?php foreach ($cabang as $c) { ?>
                                                                <option value="<?= $c->id ?>"><?= $c->cabang ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Deskripsi</label>
                                                        <textarea rows="5" class="form-control" name="deskripsi" value='<?= $barang['deskripsi']; ?>' required><?= $barang['deskripsi']; ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="gambar" name="gambar">
                                                            <label class="custom-file-label" for="gambar"><?= $barang['gambar']; ?></label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <br>
                                                            <img class="rounded" style="width:200px ;height:150px ;" src="<?= base_url() . 'assets/images/' . $barang['gambar']; ?>">

                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                    </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </tbody>
                    <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class=" col-md-1">
            </div>
        </div>
    </div>
</div>

<div id="addbarang" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() . 'pelelangan/create_barang'; ?>" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" required class="form-control" value="" name="nama_barang">
                    </div>
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" required onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control border-input" value="" name="harga_barang">
                    </div>
                    <!-- <div class="form-group">
                        <label>Tanggal Pembuka</label>
                        <input type="date" required class="form-control border-input" value="" name="tgl_buka" hidden>
                    </div> -->
                    <div class="form-group">
                        <label>Cabang Pembuka</label>
                        <select name="id_cabang" class="form-control" required>
                            <?php foreach ($cabang as $g) : ?>
                                <option value="<?= $g->id ?>"><?= $g->cabang ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control" required cols="30" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="gambar" name="gambar">
                            <label class="custom-file-label" for="gambar">Choose file</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-fill btn-wd">
                            Add Barang
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>