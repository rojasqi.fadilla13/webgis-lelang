<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <br>
    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu = "SELECT `user_menu`.`id`, `menu`
                FROM `user_menu` JOIN `user_accessmenu`
                ON `user_menu`.`id` = `user_accessmenu`.`menu_id`
                WHERE `user_accessmenu`.`role_id` = $role_id
                ORDER BY `user_accessmenu`.`menu_id` ASC
                ";
    $menu = $this->db->query($queryMenu)->result_array();
    ?>

    <!-- Menu -->
    <?php foreach ($menu as $m) : ?>
        <div class="sidebar-heading">
            <footer class="blockquote-footer"><?= $m['menu']; ?></footer>

            <!-- Submenu -->
            <?php
                $menuId = $m['id'];
                $querySubMenu = "SELECT *
                    FROM `user_submenu` JOIN `user_menu`
                    ON `user_submenu`.`menu_id` = `user_menu`.`id`
                    WHERE `user_submenu`.`menu_id` = $menuId
                    AND `user_submenu`.`is_active` = 1
                    ";
                $subMenu = $this->db->query($querySubMenu)->result_array();
                ?>

            <?php foreach ($subMenu as $sm) : ?>
                <?php if ($title == $sm['title']) : ?>
                    <li class="nav-item active">
                    <?php else : ?>
                    <li class="nav-item">
                    <?php endif; ?>
                    <a class="nav-link pb-0" href="<?= base_url($sm['url']); ?>">
                        <i class="<?= $sm['icon']; ?>"></i>
                        <span><?= $sm['title'] ?></span>
                    </a>
                    </li>
                <?php endforeach; ?>
                <hr class="mt-2">


            <?php endforeach; ?>

            <!-- <li class="nav-item">
            <a class="nav-link" href="">
                <i class="fas fa-fw fa-user"></i>
                <span>My Profile</span></a>
        </li> -->
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <h6 class="dropdown-header">Login Screens:</h6>
                    <a class="dropdown-item" href="login.html">Login</a>
                    <a class="dropdown-item" href="register.html">Register</a>
                    <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
                    <div class="dropdown-divider"></div>
                    <h6 class="dropdown-header">Other Pages:</h6>
                    <a class="dropdown-item" href="404.html">404 Page</a>
                    <a class="dropdown-item active" href="blank.html">Blank Page</a>
                </div>
            </li> -->
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('check/logout') ?>">
                    <i class="fas fa-fw fa-sign-out-alt"></i>
                    <span>Logout</span></a>
            </li>
</ul>