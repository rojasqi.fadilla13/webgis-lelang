<div class="container">

    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login Page</div>
        <div class="card-body">
            <div class="text-center">
                <h3>Welcome!</h3>
            </div><br>
            <?= $this->session->flashdata('message'); ?>
            <form class="user" method="post" action="<?= base_url('check') ?>">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="email" id="email" class="form-control" name="email" placeholder="Enter Your Email" autofocus="autofocus" value="<?= set_value('email'); ?>">
                        <label for="email">Email address</label>
                        <?php echo form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                        <label for="password">Password</label>
                        <?php echo form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Login</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="<?= base_url('check/register'); ?>">Register an Account</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
        </div>
    </div>
</div>